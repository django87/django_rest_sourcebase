from django.db import models


class Rectangle(models.Model):
    id = models.AutoField(primary_key=True)
    width = models.FloatField(null=False)
    height = models.FloatField(null=False)


class Square(models.Model):
    id = models.AutoField(primary_key=True)
    width = models.FloatField(null=False)


class Triangle(models.Model):
    id = models.AutoField(primary_key=True)
    edge1 = models.FloatField(null=False)
    edge2 = models.FloatField(null=False)
    edge3 = models.FloatField(null=False)


class Diamond(models.Model):
    id = models.AutoField(primary_key=True)
    diagonal1 = models.FloatField(null=False)
    diagonal2 = models.FloatField(null=False)
    width = models.FloatField(default=None)
