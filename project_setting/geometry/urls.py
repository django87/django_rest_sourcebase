from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views.diamond_view import DiamondDetailView, DiamondPerimeterView, DiamondListView, DiamondAreaView
from .views.triangle_view import TriangleDetailView, TrianglePerimeterView, TriangleListView, TriangleAreaView
from .views.square_view import SquareDetailView, SquarePerimeterView, SquareListView, SquareAreaView
from .views.rectangle_view import RectangleDetailView, RectanglePerimeterView, RectangleListView, RectangleAreaView



urlpatterns = [
    path('triangle/', TriangleListView.as_view()),
    path('triangle/<int:pk>/', TriangleDetailView.as_view()),
    path('triangle_area/<int:pk>/', TriangleAreaView.as_view()),
    path('triangle_perimeter/<int:pk>/', TrianglePerimeterView.as_view()),

    path('rectangle/', RectangleListView.as_view()),
    path('rectangle/<int:pk>/', RectangleDetailView.as_view()),
    path('rectangle_area/<int:pk>/', RectangleAreaView.as_view()),
    path('rectangle_perimeter/<int:pk>/', RectanglePerimeterView.as_view()),

    path('square/', SquareListView.as_view()),
    path('square/<int:pk>/', SquareDetailView.as_view()),
    path('square_area/<int:pk>/', SquareAreaView.as_view()),
    path('square_perimeter/<int:pk>/', SquarePerimeterView.as_view()),

    path('diamond/', DiamondListView.as_view()),
    path('diamond/<int:pk>/', DiamondDetailView.as_view()),
    path('diamond_area/<int:pk>/', DiamondAreaView.as_view()),
    path('diamond_perimeter/<int:pk>/', DiamondPerimeterView.as_view()),
]
