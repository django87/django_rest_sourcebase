from rest_framework import serializers
from .models import *


class TriangleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Triangle
        fields = '__all__'


class RectangleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rectangle
        fields = '__all__'


class SquareSerializer(serializers.ModelSerializer):
    class Meta:
        model = Square
        fields = '__all__'


class DiamondSerializer(serializers.ModelSerializer):
    class Meta:
        model = Diamond
        fields = '__all__'


class GeometrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Diamond
        fields = '__all__'
