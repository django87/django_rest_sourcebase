from .baseview import BaseListView, BaseDetailView
from ..models import Diamond
from ..serializers import DiamondSerializer
from .baseview import ShapeView
from rest_framework.response import Response


class DiamondListView(BaseListView):
    MODEL = Diamond
    SERIALIZER = DiamondSerializer


class DiamondDetailView(BaseDetailView):
    MODEL = Diamond
    SERIALIZER = DiamondSerializer


class DiamondAreaView(ShapeView):
    MODEL = Diamond
    SERIALIZER = DiamondSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        area = 0.5 * obj.diagonal1 * obj.diagonal2

        return Response({"area": area})


class DiamondPerimeterView(ShapeView):
    MODEL = Diamond
    SERIALIZER = DiamondSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        return Response({"perimeter": obj.width * 4})
