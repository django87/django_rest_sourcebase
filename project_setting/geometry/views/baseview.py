from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from ..models import Rectangle, Square, Triangle, Diamond
from ..serializers import RectangleSerializer, SquareSerializer, TriangleSerializer, DiamondSerializer
from django.http import Http404


class BaseListView(APIView):
    MODEL = None
    SERIALIZER = None

    def get(self, request, format=None):
        obj = self.MODEL.objects.all()
        serializer = self.SERIALIZER(obj, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = self.SERIALIZER(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BaseDetailView(APIView):
    MODEL = None
    SERIALIZER = None

    def get_object(self, pk):
        try:
            return self.MODEL.objects.get(pk=pk)
        except self.MODEL.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = self.SERIALIZER(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = self.SERIALIZER(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        obj = self.get_object(pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ShapeView(APIView):
    MODEL = None
    SERIALIZER = None

    def get_object(self, pk):
        try:
            return self.MODEL.objects.get(pk=pk)
        except self.MODEL.DoesNotExist:
            raise Http404
