from .baseview import BaseListView, BaseDetailView
from ..models import Square
from ..serializers import SquareSerializer
from .baseview import ShapeView
from rest_framework.response import Response


class SquareListView(BaseListView):
    MODEL = Square
    SERIALIZER = SquareSerializer


class SquareDetailView(BaseDetailView):
    MODEL = Square
    SERIALIZER = SquareSerializer


class SquareAreaView(ShapeView):
    MODEL = Square
    SERIALIZER = SquareSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        return Response({"area": obj.width * obj.width})


class SquarePerimeterView(ShapeView):
    MODEL = Square
    SERIALIZER = SquareSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        return Response({"perimeter": obj.width * 4})
