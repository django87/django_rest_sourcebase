from .baseview import BaseListView, BaseDetailView
from ..models import Rectangle
from ..serializers import RectangleSerializer
from .baseview import ShapeView
from rest_framework.response import Response


class RectangleListView(BaseListView):
    MODEL = Rectangle
    SERIALIZER = RectangleSerializer


class RectangleDetailView(BaseDetailView):
    MODEL = Rectangle
    SERIALIZER = RectangleSerializer


class RectangleAreaView(ShapeView):
    MODEL = Rectangle
    SERIALIZER = RectangleSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        return Response({"area": obj.height * obj.width})


class RectanglePerimeterView(ShapeView):
    MODEL = Rectangle
    SERIALIZER = RectangleSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        return Response({"perimeter": (obj.height + obj.width) * 2})
