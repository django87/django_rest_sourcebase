from .baseview import BaseListView, BaseDetailView
from ..models import Triangle
from ..serializers import TriangleSerializer
from .baseview import ShapeView
from rest_framework.response import Response


class TriangleListView(BaseListView):
    MODEL = Triangle
    SERIALIZER = TriangleSerializer


class TriangleDetailView(BaseDetailView):
    MODEL = Triangle
    SERIALIZER = TriangleSerializer


class TriangleAreaView(ShapeView):
    MODEL = Triangle
    SERIALIZER = TriangleSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        s = (obj.edge1 + obj.edge2 + obj.edge3) / 2

        area = (s * (s - obj.edge1) * (s - obj.edge2) * (s - obj.edge3)) ** 0.5

        return Response({"area": area})


class TrianglePerimeterView(ShapeView):
    MODEL = Triangle
    SERIALIZER = TriangleSerializer

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)

        return Response({"perimeter": obj.edge1 + obj.edge2 + obj.edge3})
