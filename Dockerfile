FROM python:3.11.0b4-bullseye
RUN pip install --upgrade pip

COPY ./requirements.txt .
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install cryptography

COPY ./project_setting /app

WORKDIR /app
COPY ./entrypoint.sh /
ENTRYPOINT ["sh", "/entrypoint.sh"]
